<?php
include('header.php');
if (!$_SESSION['attemptId']) {
    header('Location: /');
    die();
}
$processor = new TestAnswerProcessor();
$data = $processor->getTestResults();

?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Results</title>
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        Thank you, <?php print($data['username']) ?>! <br/><br/>
        You answered <?php print($data['correct']) ?> out of <?php print($data['overall']) ?> questions correctly.
    </body>
</html>

