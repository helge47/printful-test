<?php
include('header.php');
$testId = filter_input(INPUT_POST, 'test', FILTER_SANITIZE_NUMBER_INT);
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

if (!$testId || !$name) {
    header('Location: /');
    die();
} 

$test = new Test($testId);
$attempt = TestAttempt::makeNew($test, $name);
$_SESSION['attemptId'] = $attempt->getId();
$provider = new TestDataProvider();
$testData = $provider->getTestRepresentation($test);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Quiz</title>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <div id="quiz">
            <h1>{{ test.name }}</h1>
            <h2>Question: {{ test.questionList[currentNumber].text }}</h2>
            <form @submit.prevent="nextQuestion">
                <div class="optionsContainer">
                    <div class="option" v-for="option in test.questionList[currentNumber].answerOptionList">
                        <label class="container">{{ option.text }}
                            <input v-model.number="answer" type="radio" name="answer" :value="option.orderNumber">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="progress-bar-container">
                    <div class="progress-bar" v-bind:style="{ width: progressBarWidth }"></div>
                </div>
                <input class="submitButton" type="submit" value="Next">
                <p v-show="showPleaseSelectMessage" id="message">Please select your answer.</p>
            </form>

        </div>
        
        <script type="text/javascript">
            var test = <?php echo json_encode($testData) ?>; //passing test data to front-end
        </script>
        <script src="js/quiz.js"></script>
    </body>
</html>

