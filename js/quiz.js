
var app = new Vue({
    el: '#quiz',
    data: {
        currentNumber: 0,
        test: test, //test data representation from back-end
        answer: null,
        showPleaseSelectMessage: false
    },
    methods: {
        nextQuestion() { //method called when user clicks on "Next" button
            if (!this.answer) {
                this.showPleaseSelectMessage = true;
                return;
            }
            
            axios.post('/submitAnswer.php', {
                questionNumber: this.test.questionList[this.currentNumber].orderNumber,
                answerNumber: this.answer
            });
            
            if (this.currentNumber >= this.test.questionList.length - 1) {
                document.location.replace('/results.php');
            } else {
                this.currentNumber++;
                this.answer = null;
                this.showPleaseSelectMessage = false;
            }
        }
    },
    computed: {
        progressBarWidth: function() {
            //percentage of questions answered
            return ((this.currentNumber / this.test.questionList.length) * 100).toString() + '%';
        }
    }
});


