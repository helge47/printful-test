Author: Olegs Kurovs

Solution to technical task from Printful Latvia

In order to run the project:

1. Import database (dump.sql) using "mysql -u username -p database_name < dump.sql" command
2. Edit config.php to add your database configuration properties
3. Run the project
