-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: ok_printful_test
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attemptId` int(11) DEFAULT NULL,
  `questionNumber` int(11) DEFAULT NULL,
  `answerNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aid` (`attemptId`),
  CONSTRAINT `Answer_ibfk_1` FOREIGN KEY (`attemptId`) REFERENCES `TestAttempt` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answer`
--

LOCK TABLES `Answer` WRITE;
/*!40000 ALTER TABLE `Answer` DISABLE KEYS */;
INSERT INTO `Answer` VALUES (30,115,1,1),(31,115,2,2),(32,115,3,2),(33,116,1,2),(34,116,2,3);
/*!40000 ALTER TABLE `Answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnswerOption`
--

DROP TABLE IF EXISTS `AnswerOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnswerOption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `questionId` int(11) DEFAULT NULL,
  `isCorrect` tinyint(1) DEFAULT NULL,
  `orderNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qid_orn` (`questionId`,`orderNumber`),
  KEY `qid` (`questionId`),
  CONSTRAINT `AnswerOption_ibfk_1` FOREIGN KEY (`questionId`) REFERENCES `Question` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnswerOption`
--

LOCK TABLES `AnswerOption` WRITE;
/*!40000 ALTER TABLE `AnswerOption` DISABLE KEYS */;
INSERT INTO `AnswerOption` VALUES (1,'2',1,0,1),(2,'3',1,0,2),(3,'4',1,1,3),(4,'42',2,0,1),(5,'56',2,1,2),(6,'25',3,0,1),(7,'125',3,1,2),(8,'625',3,0,3),(9,'Paris',4,0,1),(10,'London',4,1,2),(11,'Riga',4,0,3),(12,'Cambodia',5,0,1),(13,'Latvia',5,0,2),(14,'Lithuania',5,1,3),(15,'Estonia',5,0,4),(16,'120',2,0,4),(17,'68',2,0,5),(18,'72',2,0,6),(19,'78',2,0,7);
/*!40000 ALTER TABLE `AnswerOption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Question`
--

DROP TABLE IF EXISTS `Question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `testId` int(11) DEFAULT NULL,
  `orderNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `testId` (`testId`,`orderNumber`),
  KEY `tid` (`testId`),
  CONSTRAINT `Question_ibfk_1` FOREIGN KEY (`testId`) REFERENCES `Test` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question`
--

LOCK TABLES `Question` WRITE;
/*!40000 ALTER TABLE `Question` DISABLE KEYS */;
INSERT INTO `Question` VALUES (1,'2 + 2 = ?',1,1),(2,'7 * 8 = ?',1,2),(3,'5^3 = ?',1,3),(4,'Capital of the UK',2,1),(5,'Vilnius is the capital of..',2,2);
/*!40000 ALTER TABLE `Question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test`
--

DROP TABLE IF EXISTS `Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test`
--

LOCK TABLES `Test` WRITE;
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
INSERT INTO `Test` VALUES (1,'Arithmetics'),(2,'Geography');
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TestAttempt`
--

DROP TABLE IF EXISTS `TestAttempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TestAttempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testId` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `result` int(11) DEFAULT '0',
  `isFinished` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `testId` (`testId`),
  CONSTRAINT `TestAttempt_ibfk_1` FOREIGN KEY (`testId`) REFERENCES `Test` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TestAttempt`
--

LOCK TABLES `TestAttempt` WRITE;
/*!40000 ALTER TABLE `TestAttempt` DISABLE KEYS */;
INSERT INTO `TestAttempt` VALUES (115,1,'Olegs',2,1),(116,2,'Olegs',2,1);
/*!40000 ALTER TABLE `TestAttempt` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-27  0:58:39
