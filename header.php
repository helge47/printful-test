<?php

spl_autoload_register(function ($className) {
    require_once(__DIR__.'/Classes/'.$className.'.php');
});
session_start();
require_once('config.php');