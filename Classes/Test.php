<?php

class TestInvalidIdException extends Exception {};

/*
 * class representing a single test (quiz)
 */
final class Test {
    const TABLE_NAME = 'Test';
    
    private $id;
    private $name;
    private $questionList; //array of Question objects
    
    public function __construct(int $id) {
        $this->id = $id;
        $this->fetchFromDb();
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getQuestionList() {
        return $this->questionList;
    }
    
    public function getQuestionNumber() {
        return count($this->questionList);
    }
    
    private function fetchFromDb() {
        $db = new Database();
        $query = 'SELECT * FROM '.self::TABLE_NAME.
                '   WHERE id = '.$this->id.'; ';
        $res = $db->query($query);
        
        if ($row = $res->fetch_assoc()) {
            $this->name = $row['name'];
        } else {
            throw new TestInvalidIdException;
        }
        
        $this->questionList = Question::makeQuestionListByTest($this);
    }    
}
