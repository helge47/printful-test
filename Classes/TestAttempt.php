<?php

class TestAttemptInvalidIdException extends Exception {};

/*
 * Represents a single attempt on taking a test
 * Each time a user starts a test, a new attempt is created
 */
final class TestAttempt {
    const TABLE_NAME = 'TestAttempt';
    const ANSWER_TABLE_NAME = 'Answer';
    private $id;
    private $testId; //id of taken test
    private $username; //name user provided on the index page
    private $result = 0; //number of correct answers
    private $isFinished = false; 
    
    public function __construct(int $id, int $testId = null, string $username = null) {
        $this->id = $id;
        if (!$testId || !$username) {
            $this->fetchFromDb();
        }
    }
    
    public function getId() {
        return $this->id;
    }
    
    public static function makeNew(Test $test, string $username) {
        $db = new Database();
        $query = 'INSERT INTO '.self::TABLE_NAME.
                '   (testId, username) '.
                'VALUES ('.
                '   '.$test->getId().', '.
                '   \''.$username.'\' '.
                ');';
        $db->query($query);
        return new self($db->getInsertId(), $test->getId(), $username);
    }
    
    /*
     * stores user's answer to a question
     * called every time user submits an answer
     */
    public static function storeAnswer(int $attemptId, int $questionNumber, int $answerNumber) {
        $db = new Database();
        $query = 'INSERT INTO '.self::ANSWER_TABLE_NAME.
                '   (attemptId, questionNumber, answerNumber) '.
                'VALUES ('.
                '   '.$attemptId.', '.
                '   '.$questionNumber.', '.
                '   '.$answerNumber.' '.
                ');';
        $db->query($query);
    }
    
    /*
     * called after the whole test is finished
     * queries for all given answers, and calculates the number of correct ones
     */
    private function processFinalResults() {
        $db = new Database();
        
        $query = 'SELECT AnswerOption.id, AnswerOption.isCorrect FROM Answer '.
                '   JOIN TestAttempt '.
                '       ON TestAttempt.id = Answer.attemptId '.
                '   JOIN Question '.
                '       ON Question.orderNumber = Answer.questionNumber '.
                '       AND Question.testId = TestAttempt.testId '.
                '   JOIN AnswerOption '.
                '       ON AnswerOption.questionId = Question.id '.
                '       AND AnswerOption.orderNumber = Answer.answerNumber '.
                'WHERE TestAttempt.id = '.$this->id.
                ';  ';
        
        $res = $db->query($query);
        $correctNumber = 0;
        
        while ($row = $res->fetch_assoc()) {
            if ($row['isCorrect']) {
                $correctNumber++;
            }
        }
        
        $this->result = $correctNumber;
        $this->isFinished = TRUE;
        $this->saveToDb();
    }
    
    /*
     * provides results of the test attempt
     */
    public function getResultsData() {
        if (!$this->isFinished) {
            $this->processFinalResults();
        }
        
        $test = new Test($this->testId);
        $overallNumber = $test->getQuestionNumber();
        
        return [
            'username' => $this->username,
            'correct' => $this->result,
            'overall' => $overallNumber
        ];
    }
    
    private function fetchFromDb() {
        $db = new Database();
        $query = 'SELECT * FROM '.self::TABLE_NAME.' '.
                '   WHERE id = '.$this->id.'; ';
        $res = $db->query($query);
        
        if ($row = $res->fetch_assoc()) {
            $this->testId = $row['testId'];
            $this->username = $row['username'];
            $this->result = $row['result'];
            $this->isFinished = $row['isFinished'];
        } else {
            throw new TestAttemptInvalidIdException;
        }
    }
    
    /*
     * saves object to database
     */
    private function saveToDb() {
        $db = new Database();
        $query = 'UPDATE '.self::TABLE_NAME.' '.
                '   SET '.
                '       testId = '.$this->testId.', '.
                '       username = \''.$this->username.'\', '.
                '       result = '.$this->result.', '.
                '       isFinished = '.(int)$this->isFinished.' '.
                '   WHERE id = '.$this->id.' '.
                '   ;';
        $db->query($query);
    }
}
