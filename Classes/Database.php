<?php

final class DatabaseFailedConnectionException extends Exception {};
final class DatabaseQueryErrorException extends Exception {};

/*
 * A wrapper class for mysqli database connection object
 */
final class Database {
    protected static $connection;
    
    public function __construct() {
        if (!self::$connection) {     
            global $config;
            self::$connection = new mysqli($config['db_host'], $config['db_user'], $config['db_password'], $config['db_name']);

            if (self::$connection->connect_errno) {
                self::$connection = null;
                throw new DatabaseFailedConnectionException(self::$connection->connect_error);
            }
        }
    }
    
    public function query(string $sqlQuery) {
        $res = self::$connection->query($sqlQuery);
        
        if (self::$connection->errno) {
            throw new DatabaseQueryErrorException(self::$connection->error.self.$sqlQuery);
        }
        
        return $res;
    }
    
    /*
     * returns id of the last inserted row
     */
    public function getInsertId() {
        return self::$connection->insert_id;
    }
}
