<?php

/*
 * middleware class for question answer submission and result calculation
 */
class TestAnswerProcessor {
    public function submitAnswer(int $questionNumber, int $answerNumber) {
        $attemptId = $_SESSION['attemptId'];
        TestAttempt::storeAnswer($attemptId, $questionNumber, $answerNumber);
    }
    
    public function getTestResults() {
        $attemptId = $_SESSION['attemptId'];
        $attempt = new TestAttempt($attemptId);
        return $attempt->getResultsData();
    }
}
