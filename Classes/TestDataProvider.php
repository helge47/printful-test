<?php

final class TestDataProvider {
    public function getTestNameList() {
        $db = new Database();
        $query = 'SELECT * FROM '.Test::TABLE_NAME.'; ';
        $res = $db->query($query);
        $nameList = array();
        
        while ($row = $res->fetch_assoc()) {
            $nameList[] = [
                'id' => $row['id'],
                'name' => $row['name']
                ];
        }
        
        return $nameList;
    }
    
    public function getTestRepresentation(Test $test) {
        $questions = $test->getQuestionList();
        $list = [];
        
        foreach ($questions as $q) {
            $list[] = $q->getExternalRepresentation();
        }
        
        return [
            'id' => $test->getId(),
            'name' => $test->getName(),
            'questionList' => $list
        ];
    }
}
