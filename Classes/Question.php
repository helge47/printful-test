<?php

class QuestionInvalidIdException extends Exception {};
/*
 * Class represents a single test question.
 */
final class Question {
    const TABLE_NAME = 'Question'; 
    const ANSWER_TABLE_NAME = 'AnswerOption';
    
    private $id;
    private $text; //question text
    private $orderNumber; //question order number in test
    private $answerOptionList; //array of arrays containing public answer option info
    
    public function __construct(int $id, string $text = '', int $orderNumber = null) {
        $this->id = $id;
        
        if (!$text || !$orderNumber) {
            $this->fetchFromDb();
        } else {
            $this->text = $text;
            $this->orderNumber = $orderNumber;
        }
        
        $this->fetchAnswersFromDb();
    }
    
    /*
     * creates a list of all questions in a test
     */
    public static function makeQuestionListByTest(Test $test) {
        $db = new Database();
        $query = 'SELECT * FROM '.self::TABLE_NAME.
                '   WHERE testId = '.$test->getId().'; ';
        $res = $db->query($query);
        
        $list = [];
        while ($row = $res->fetch_assoc()) {
            $list[] = new Question($row['id'], $row['text'], $row['orderNumber']);
        }
        
        return $list;
    }
    
    /*
     * returns an array containg public info about the question
     * which is later passed to the front-end
     */
    public function getExternalRepresentation() {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'orderNumber' => $this->orderNumber,
            'answerOptionList' => $this->answerOptionList
        ];
    }
    
    private function fetchFromDb() {
        $db = new Database();
        $query = 'SELECT * FROM '.self::TABLE_NAME.
                '   WHERE id = '.$this->id.'; ';
        $res = $db->query($query);
        
        if ($row = $res->fetch_assoc()) {
            $this->text = $row['text'];
            $this->orderNumber = $row['orderNumber'];
        } else {
            throw new QuestionInvalidIdException;
        }
    }
    
    /*
     * fills $this->answerOptionList with info about all 
     * answer options for this question
     */
    private function fetchAnswersFromDb() {
        $db = new Database();
        $query = 'SELECT * FROM '.self::ANSWER_TABLE_NAME.
                '   WHERE questionId = '.$this->id.'; ';
        $res = $db->query($query);
        
        while ($row = $res->fetch_assoc()) {
            $this->answerOptionList[] = [
                'id' => $row['id'],
                'orderNumber' => $row['orderNumber'],
                'text' => $row['text']
            ];
        }
    }
}
