<?php
include('header.php');
require_once('Classes/TestDataProvider.php');
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Welcome to Test Website!</title>
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <div class="mainTitle">Welcome to the Testing Website.</div>
        <div class="description"> Please select a test you want to take.</div>
        <form method="POST" action="quiz.php">
            <input class="textInput" type="text" placeholder="Your Name" name="name"><br/>
            <select name="test">
                <option selected disabled>Choose test</option>
                <?php
                    $provider = new TestDataProvider();
                    $list = $provider->getTestNameList();
                    
                    foreach ($list as $test) {
                ?>
                        <option value="<?php print($test['id']) ?>"><?php print($test['name']) ?></option>
                <?php
                    }
                ?>
            </select><br/>
            <input class="submitButton" type="submit" value="Start test">
        </form>
    </body>
</html>
