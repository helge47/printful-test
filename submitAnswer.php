<?php

require_once('header.php');
$data = json_decode(file_get_contents('php://input'), TRUE);
$questionNumber = filter_var($data['questionNumber'], FILTER_SANITIZE_NUMBER_INT);
$answerNumber = filter_var($data['answerNumber'], FILTER_SANITIZE_NUMBER_INT);

$processor = new TestAnswerProcessor();
$processor->submitAnswer($questionNumber, $answerNumber);
